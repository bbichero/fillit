/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_all_form.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jde-mot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 11:37:52 by jde-mot           #+#    #+#             */
/*   Updated: 2016/11/24 11:17:21 by jde-mot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/fillit.h"

int			ft_all_form(t_file *tetri)
{
	int		i;
	int		k;

	i = 0;
	k = 1;
	while (tetri)
	{
		if (ft_nbligne(tetri) != 1 && ft_nbligne(tetri) != 2
				&& ft_nbligne(tetri) != 3 && ft_nbligne(tetri) != 0)
			k--;
		if (ft_nbligne(tetri) == 1)
			if (ft_form_cmp(tetri) == 0)
				k--;
		if (ft_nbligne(tetri) == 2)
			if (ft_form_ligncmp(tetri) == 0)
				k--;
		if (ft_nbligne(tetri) == 3)
			if (ft_form_lignv(tetri) == 0)
				k--;
		if (ft_nbligne(tetri) == 0)
			if (ft_lignh(tetri) == 0)
				k--;
		tetri = tetri->next;
	}
	return (k);
}
