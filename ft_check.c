/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jde-mot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 11:29:31 by jde-mot           #+#    #+#             */
/*   Updated: 2016/11/24 12:45:01 by jde-mot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/fillit.h"

int			ft_nbligne(t_file *tetri)
{
	int		i;
	int		j;
	int		k;

	i = 0;
	j = 0;
	k = 0;
	while (i <= 20)
	{
		if (tetri->line[i] == '#')
		{
			while (j < 4)
			{
				if (tetri->line[i] == '#')
					j++;
				if (tetri->line[i] == '\n')
					k++;
				i++;
			}
		}
		i++;
	}
	return (k);
}

int		ft_len(t_file *tetri)
{
	t_file		*tmp;

	tmp = tetri;
	while (tmp)
	{
		if (ft_strlen(tmp->line) != 21)
		{
			if (ft_strlen(tmp->line) == 20 && tmp->next == NULL)
				return (1);
			write(1,"error\n", 7);
		}
		tmp = tmp->next;
	}
	return (0);
}

int		ft_check2(t_file *tetri)
{
	t_file		*tmp;

	tmp = tetri;
	if (ft_len(tmp) == 0)
	{
		write(1, "error\n",7);
		return (0);
	}
	tmp = tetri;
	if (ft_all_form(tmp) == 1)
		return (1);
	write(1, "error\n", 7);
	return (0);
}

int			ft_check(t_file *file)
{
	t_file		*tmp;

	tmp = file;
	if (ft_checknbl(tmp) == 0)
		return (0);
	tmp = file;
	if (ft_checknbp(tmp) == 0)
		return (0);
	return (1);
}
