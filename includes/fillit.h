/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 13:09:23 by bbichero          #+#    #+#             */
/*   Updated: 2016/11/24 10:49:34 by jde-mot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# include "libft/libft.h"
# include <fcntl.h>
# include <stdio.h>

typedef struct		s_file
{
	char			*line;
	struct s_file	*next;
}					t_file;

typedef struct		s_map
{
	char			*line;
	struct s_map	*next;
}					t_map;
/*
typedef struct		s_pos
{
	int				x;
    int             y;
	struct s_pos	*next;
}					t_pos;
*/
typedef struct		s_pos
{
	int				**pos;
	struct s_pos	*next;
}					t_pos;

typedef struct		s_fillit
{
	struct s_file	*file;
	struct s_map	*smap;
}					t_fillit;

t_file				*ft_file_ini(t_file *file);
void				ft_fileadd(t_file **alst, t_file *new);
t_fillit			*ft_fillit_ini(t_fillit *fillit);
t_file				*ft_create_map(char *file_name, t_file *file);
t_file				*ft_maketretiminos(t_file *file);
int					ft_checknbp(t_file *file);
int					ft_checknbl(t_file *file);
int					ft_check2(t_file *tetri);
int					ft_all_form(t_file *tetri);
int					ft_check(t_file *file);
int					ft_nbligne(t_file *tetri);
int					ft_form_cmp(t_file *tetri);
int					ft_form_ligncmp(t_file *tetri);
int					ft_form_lignv(t_file *tetri);
int					ft_lignh(t_file *tetri);
int					ft_nbligne(t_file *tetri);
int					ft_len(t_file *tetri);
int					ft_lstsize(t_file *begin_list);
t_pos				*ft_inipos(t_file *tetri);
/*-------------------------*/
void	ft_lignpos1(t_pos *position, int i);
void	ft_lignpos2(t_pos *position, int i);
void	ft_lignpos3(t_pos *position, int i);
t_pos		*ft_makepos(t_file *tetri);
void		ft_lignposhv(t_pos *position, int i);
void		ft_makeposligncmp(t_pos *position, t_file *tetri);
void	ft_lignpos4(t_pos *position, int i);
void		ft_makeposcmp(t_pos *position, t_file *tetri);
void	ft_cmppos1(t_pos *position);
void	ft_cmppos2(t_pos *position, int i);
void	ft_cmppos3(t_pos *position, int i);
void	ft_cmppos4(t_pos *position, int i);
void	ft_cmppos5(t_pos *position, int i);

#endif
