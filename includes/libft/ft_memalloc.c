/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <bbichero@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/10 11:02:01 by bbichero          #+#    #+#             */
/*   Updated: 2015/04/14 09:17:43 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void				*ft_memalloc(size_t size)
{
	unsigned int	i;
	void			*str;
	char			*string;

	i = 0;
	if (size == 0)
		return (NULL);
	str = (void *)malloc(sizeof(void) * size);
	string = (char *)str;
	if (str == NULL)
		return (NULL);
	while (i++ < size)
		string[i] = 0;
	return (str);
}
