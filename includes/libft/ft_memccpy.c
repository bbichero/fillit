/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <bbichero@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/10 11:52:12 by bbichero          #+#    #+#             */
/*   Updated: 2014/11/11 16:24:32 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*ft_memccpy(void *src, const void *dest, int c, size_t n)
{
	const char	*d;
	char		*s;

	d = dest;
	s = src;
	while (n--)
	{
		*s++ = *d;
		if (*d == c)
			return (s);
		d++;
	}
	return (0);
}
