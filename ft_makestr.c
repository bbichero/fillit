/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_makestr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jde-mot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 12:32:40 by jde-mot           #+#    #+#             */
/*   Updated: 2016/11/24 10:12:35 by jde-mot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/fillit.h"

t_file		*ft_maketretiminos(t_file *file)
{
	t_file		*pieces;
	t_file		*tmp;
	int			i;

	i = 1;
	tmp = (t_file *)malloc(sizeof(t_file));
	tmp->line = file->line;
	pieces = tmp;
	file = file->next;
	while (file->next)
	{
		tmp->line = ft_strjoin(tmp->line, file->line);
		if (i == 4)
		{
			tmp->next = (t_file *)malloc(sizeof(t_file));
			tmp = tmp->next;
			file = file->next;
			tmp->line = file->line;
			i = 0;
		}
		i++;
		file = file->next;
	}
	tmp->next = NULL;
	return (pieces);
}

t_file			*ft_create_map(char *file_name, t_file *file)
{
	int			fd;
	char		*line;
	int			ret;
	t_file		*tmp;

	file = (t_file *)malloc(sizeof(t_file));
	fd = open(file_name, O_RDONLY);
	if (fd == -1)
	{
		ft_putstr("open() error");
		return (0);
	}
	tmp = file;
	while ((ret = get_next_line((int const)fd, &line)) > 0)
	{
		file->line = ft_strdup(line);
		file->line = ft_strjoin(file->line, "\n");
		file->next = (t_file *)malloc(sizeof(t_file));
		file = file->next;
	}
	file->next = NULL;
	return (tmp);
}
