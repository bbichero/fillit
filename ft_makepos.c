/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_makepos.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jde-mot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 11:16:49 by jde-mot           #+#    #+#             */
/*   Updated: 2016/11/24 09:45:44 by jde-mot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/fillit.h"

t_pos		*ft_makepos(t_file *tetri)
{
	t_pos		*position;
	t_pos		*tmp;


	tmp = ft_inipos(tetri);
	position = tmp;
	while (tetri)
	{
		if (ft_nbligne(tetri) == 0)
			ft_lignposhv(tmp, 0);
		if (ft_nbligne(tetri) == 3)
			ft_lignposhv(tmp, 3);
		if (ft_nbligne(tetri) == 2)
			ft_makeposligncmp(tmp, tetri);
		if (ft_nbligne(tetri) == 1)
			ft_makeposcmp(tmp, tetri);
		tmp = tmp->next;
		tetri = tetri->next;
	}















	return (position);
}
