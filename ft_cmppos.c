/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cmppos.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jde-mot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/24 09:33:02 by jde-mot           #+#    #+#             */
/*   Updated: 2016/11/24 12:38:41 by jde-mot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/fillit.h"

void	ft_cmppos1(t_pos *position)
{
		position->pos[0][0] = 1;
		position->pos[0][1] = 1;
		position->pos[1][0] = 1;
		position->pos[1][1] = 1;
}

void	ft_cmppos2(t_pos *position, int i)
{
	if (i == 1)
	{
		position->pos[0][0] = 1;
		position->pos[0][1] = 1;
		position->pos[1][1] = 1;
		position->pos[1][2] = 1;
	}
	if (i == 2)
	{
		position->pos[0][1] = 1;
		position->pos[0][2] = 1;
		position->pos[1][0] = 1;
		position->pos[1][1] = 1;
	}
}

void	ft_cmppos3(t_pos *position, int i)
{
	if (i == 1)
	{
		position->pos[0][1] = 1;
		position->pos[1][0] = 1;
		position->pos[1][1] = 1;
		position->pos[1][2] = 1;
	}
	if (i == 2)
	{
		position->pos[0][0] = 1;
		position->pos[0][1] = 1;
		position->pos[0][2] = 1;
		position->pos[1][1] = 1;
	}
}

void	ft_cmppos4(t_pos *position, int i)
{
	if (i == 1)
	{
		position->pos[0][0] = 1;
		position->pos[0][1] = 1;
		position->pos[0][2] = 1;
		position->pos[1][0] = 1;
	}
	if (i == 2)
	{
		position->pos[0][0] = 1;
		position->pos[0][1] = 1;
		position->pos[0][2] = 1;
		position->pos[1][2] = 1;
	}
}

void	ft_cmppos5(t_pos *position, int i)
{
	if (i == 1)
	{
		position->pos[0][0] = 1;
		position->pos[1][0] = 1;
		position->pos[1][1] = 1;
		position->pos[1][2] = 1;
	}
	if (i == 2)
	{
		position->pos[0][2] = 1;
		position->pos[1][0] = 1;
		position->pos[1][1] = 1;
		position->pos[1][2] = 1;
	}
}
