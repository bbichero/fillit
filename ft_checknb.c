/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_checknb.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jde-mot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 12:44:29 by jde-mot           #+#    #+#             */
/*   Updated: 2016/11/24 11:08:14 by jde-mot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/fillit.h"

int			ft_checknbl(t_file *file)
{
	int		i;

	i = 0;
	while (file->next)
	{
		if ((ft_strlen(file->line) < 5) && (ft_strlen(file->next->line) < 5))
			return (0);
		file = file->next;
	}
	return (1);
}

int			ft_checknbp(t_file *file)
{
	int		i;
	int		j;

	j = 0;
	while (file->next)
	{
		i = 0;
		while (file->line[i])
		{
			if (i != 0 && file->line[i] == '\n' && file->line[i - 1] != '\n')
				j++;
			i++;
		}
		file = file->next;
	}
	j = j / 4;
	if (j > 26)
		return (0);
	return (j);
}
