/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_form_lign.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jde-mot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 11:40:19 by jde-mot           #+#    #+#             */
/*   Updated: 2016/11/24 11:16:12 by jde-mot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/fillit.h"

int			ft_form_lignv(t_file *tetri)
{
	int		i;
	int		j;
	int		k;

	i = 0;
	j = 0;
	k = 0;
	while (k++ <= 20)
	{
		if (i < 4 && tetri->line[i] == '#' && j < 3)
		{
			j++;
			if (tetri->line[i + 5] == '#')
				return (1);
		}
		i++;
	}
	return (0);
}

int			ft_form_ligncmp(t_file *tetri)
{
	int		i;
	int		j;
	int		k;
	int		l;

	i = 0;
	j = 0;
	k = 0;
	l = 0;
	while (i++ <= 20)
	{
		if (tetri->line[i] == '#')
		{
			while (j++ < 12 && l < 4)
			{
				if (tetri->line[i] == '#')
					l++;
				k = k + (unsigned char)tetri->line[i];
				i++;
			}
		}
	}
	if (k == 344 || k == 390 || k == 436)
		return (1);
	return (0);
}

int			ft_lignh(t_file *tetri)
{
	int		i;
	int		j;
	int		k;

	i = 0;
	j = 0;
	k = 0;
	while (i <= 20)
	{
		if (tetri->line[i] == '#')
		{
			while (j < 4)
			{
				k = k + (unsigned char)tetri->line[i];
				j++;
				i++;
			}
		}
		i++;
	}
	if (k == 140)
		return (1);
	return (0);
}

int			ft_form_cmp(t_file *tetri)
{
	int		i;
	int		j;
	int		k;

	i = 0;
	j = 0;
	k = 0;
	while (i <= 20)
	{
		if (tetri->line[i] == '#')
		{
			while (j < 7)
			{
				k = k + (unsigned char)tetri->line[i];
				j++;
				i++;
			}
		}
		i++;
	}
	if (k == 206 || k == 242 || k == 253)
		return (1);
	return (0);
}
