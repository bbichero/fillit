/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lignpos.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jde-mot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/24 09:33:19 by jde-mot           #+#    #+#             */
/*   Updated: 2016/11/24 10:08:03 by jde-mot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/fillit.h"

void	ft_lignpos1(t_pos *position, int i)
{
	if (i == 1)
	{
		position->pos[0][0] = 1;
		position->pos[0][1] = 1;
		position->pos[1][0] = 1;
		position->pos[2][0] = 1;
	}
	if (i == 2)
	{
		position->pos[0][0] = 1;
		position->pos[0][1] = 1;
		position->pos[1][1] = 1;
		position->pos[2][1] = 1;
	}
}

void	ft_lignpos2(t_pos *position, int i)
{
	if (i == 1)
	{
		position->pos[0][0] = 1;
		position->pos[1][0] = 1;
		position->pos[1][1] = 1;
		position->pos[2][0] = 1;
	}
	if (i == 2)
	{
		position->pos[0][1] = 1;
		position->pos[1][0] = 1;
		position->pos[1][1] = 1;
		position->pos[2][1] = 1;
	}
}

void	ft_lignpos3(t_pos *position, int i)
{
	if (i == 1)
	{
		position->pos[0][0] = 1;
		position->pos[1][0] = 1;
		position->pos[2][0] = 1;
		position->pos[2][1] = 1;
	}
	if (i == 2)
	{
		position->pos[0][1] = 1;
		position->pos[1][1] = 1;
		position->pos[2][1] = 1;
		position->pos[2][0] = 1;
	}
}

void	ft_lignpos4(t_pos *position, int i)
{
	if (i == 1)
	{
		position->pos[0][1] = 1;
		position->pos[1][0] = 1;
		position->pos[1][1] = 1;
		position->pos[2][0] = 1;
	}
	if (i == 2)
	{
		position->pos[0][0] = 1;
		position->pos[1][0] = 1;
		position->pos[1][1] = 1;
		position->pos[2][1] = 1;
	}
}

void		ft_lignposhv(t_pos *position, int i)
{
	if (i == 0)
	{
		position->pos[0][0] = 1;
		position->pos[0][1] = 1;
		position->pos[0][2] = 1;
		position->pos[0][3] = 1;
	}
	if (i == 3)
	{
		position->pos[0][0] = 1;
		position->pos[1][0] = 1;
		position->pos[2][0] = 1;
		position->pos[3][0] = 1;
	}
}
