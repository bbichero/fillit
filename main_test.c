/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_test.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jde-mot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/17 07:32:16 by jde-mot           #+#    #+#             */
/*   Updated: 2016/11/24 12:45:38 by jde-mot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/fillit.h"

void				ft_fillit(t_pos *test)
{
	int			i;
	int			j;
	while (test)
	{
		i = 0;
		while (test->pos[i])
		{
		printf("\n");
			j = 0;
			while (j < 4)
			{
				printf("%i",test->pos[i][j]);
				j++;
			}
			i++;
		}
		printf("\n");
		test = test->next;
	}
}

int				main(int argc, char **argv)
{
	t_file		*tetri;
	t_file		*file;
	t_pos		*tmp;

	file = NULL;
	tetri = NULL;
	if (argc != 2)
	{
		write(1, "usage: fillit file\n", 19);
		return (0);
	}
	file = ft_create_map(argv[1], file);
	if (ft_check(file) == 0)
		write(1, "error\n", 7);
	else
	{
		tetri = ft_maketretiminos(file);
		if (ft_check2(tetri) == 0)
			return (0);
	}
	tmp = ft_makepos(tetri);
	ft_fillit(tmp);

	return (0);
}
