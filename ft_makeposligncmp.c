/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_makeposligncmp.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jde-mot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 12:14:22 by jde-mot           #+#    #+#             */
/*   Updated: 2016/11/24 10:52:21 by jde-mot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/fillit.h"

void		ft_cond1(t_pos *position, t_file *tetri, int i)
{
	if (tetri->line[i + 5] == '#')
		ft_lignpos1(position, 1);
	else
		ft_lignpos1(position, 2);
}

void		ft_cond2(t_pos *position, t_file *tetri, int i)
{
	if (tetri->line[i + 9] == '#')
		ft_lignpos4(position, 1);
	else
		ft_lignpos4(position, 2);
}

void		ft_cond3(t_pos *position, t_file *tetri, int i)
{
	if (tetri->line[i + 4] == '#')
	{
		ft_lignpos2(position, 2);
		return;
	}
	if (tetri->line[i + 6] == '#')
	{
		ft_lignpos2(position, 1);
		return;
	}
	if (tetri->line[i + 9] == '#')
		ft_lignpos3(position, 2);
	else
		ft_lignpos3(position, 1);
}

void		ft_makeposligncmp(t_pos *position, t_file *tetri)
{
	int		i;

	i = 0;
	while (tetri->line[i])
	{
		if (tetri->line[i] == '#')
		{
			if (tetri->line[i + 1] == '#')
			{
				ft_cond1(position, tetri, i);
				return;
			}
			if (tetri->line[i + 10] == '.')
			{
				ft_cond2(position, tetri, i);
				return;
			}
			if (tetri->line[i + 5] == '#')
			{
				ft_cond3(position, tetri, i);
				return;
			}
		}
		i++;
	}
}
