/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_inipos.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jde-mot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 11:51:58 by jde-mot           #+#    #+#             */
/*   Updated: 2016/11/24 10:11:39 by jde-mot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/fillit.h"

int			ft_lstsize(t_file *begin_list)
{
	int		i;
	t_file	*elem;

	i = 0;
	elem = begin_list;
	while (elem != NULL)
	{
		i++;
		elem = elem->next;
	}
	return (i);
}

t_pos		*ft_inistr(t_file *tetri)
{
	int		i;
	t_pos	*position;
	t_pos	*save;

	i = ft_lstsize(tetri) - 1;
	position = (t_pos *)malloc(sizeof(t_pos));
	save = position;
	while (i-- > 0)
	{
		position->next = (t_pos *)malloc(sizeof(t_pos));
		position = position->next;
	}
	position->next = NULL;
	return (save);
}

t_pos		*ft_inipos(t_file *tetri)
{
	t_pos	*save;
	t_pos	*position;
	int		j;
	int		k;

	position = ft_inistr(tetri);
	save = position;
	while (position)
	{
		j = -1;
		position->pos = (int **)malloc(sizeof(int *) * 4);
		while (j++ < 3)
		{
			k = -1;
			while (k++ < 3)
			{
				position->pos[j] = (int *)malloc(sizeof(int) * 3);
				position->pos[j][k] = 0;
			}
		}
		position->pos[j] = 0;
		position = position->next;
	}
	return (save);
}
