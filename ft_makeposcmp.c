/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_makeposcmp.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jde-mot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 12:14:14 by jde-mot           #+#    #+#             */
/*   Updated: 2016/11/24 12:48:14 by jde-mot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/fillit.h"

void		ft_cmpcond1(t_pos *position, t_file *tetri, int i)
{
	if (tetri->line[i + 2] == '#')
	{
		if (tetri->line[i + 5] == '#')
			ft_cmppos4(position, 1);
		if (tetri->line[i + 6] == '#')
			ft_cmppos3(position, 2);
		if (tetri->line[i + 7] == '#')
			ft_cmppos4(position, 2);
		return;
	}
	if (tetri->line[i + 4] == '#')
		ft_cmppos2(position, 2);
	if (tetri->line[i + 5] == '#' && tetri->line[i + 6] == '#')
		ft_cmppos1(position);
	if (tetri->line[i + 6] == '#' && tetri->line[i + 7] == '#')
		ft_cmppos2(position, 1);
}

void		ft_makeposcmp(t_pos *position, t_file *tetri)
{
	int		i;

	i = 0;
	while (tetri->line[i])
	{
		if (tetri->line[i] == '#')
		{
			if (tetri->line[i + 1] == '#')
			{
				ft_cmpcond1(position, tetri, i);
				return;
			}
			if (tetri->line[i + 3] == '#')
			{
				ft_cmppos5(position, 2);
				return;
			}
			if (tetri->line[i + 4] == '#')
			{
				ft_cmppos3(position, 1);
				return;
			}
				ft_cmppos5(position, 1);
		}
		i++;
	}
}
